#include "filesystem.h"
#include <sys/stat.h>
#include <sys/types.h>
#include "debug.h"

#ifdef _WIN32
#include <Windows.h>
#include <direct.h>
#elif __linux__
#include <unistd.h>
#endif

namespace utility
{
	std::string FileSystem::_executableDirectory;

	std::string FileSystem::getExecutableDirectory()
	{
		if(_executableDirectory.length() == 0)
		{
	#ifdef _WIN32
			char buf[MAX_PATH];
			GetModuleFileName(NULL, buf, MAX_PATH);
			std::string::size_type pos = std::string(buf).find_last_of("\\/");
			_executableDirectory = std::string(buf).substr(0, pos + 1);
	#elif __linux__
			char cdir[FILENAME_MAX];
			if (!readlink("/proc/self/exe", cdir,sizeof(cdir)))
			{
				_executableDirectory = "";
			}
			cdir[sizeof(cdir) - 1] = '\0';
			std::string::size_type pos = std::string(cdir).find_last_of('/');
			_executableDirectory = std::string(cdir).substr(0, pos + 1);
	#endif
		}

		return _executableDirectory;
	}

	bool FileSystem::directoryExists(const char* directory)
	{
		struct stat info;

		if(stat(directory, &info) != 0)
		{
			return false;
		}
		else if(info.st_mode & S_IFDIR)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	bool FileSystem::makeDirectory(const char* directory)
	{
		#ifdef _WIN32
		return _mkdir(directory) == 0;
		#elif __linux__
		return mkdir(directory, 0777) == 0;
		#endif
	}

	#include "tinydir.h"

	std::vector<std::string> FileSystem::getFilesInDirectory(const char* directory, bool includeDirs)
	{
		std::vector<std::string> res;
		tinydir_dir dir;
		tinydir_open(&dir, directory);

		while (dir.has_next)
		{
			tinydir_file file;
			tinydir_readfile(&dir, &file);

			if (!file.is_dir || includeDirs)
			{
				res.push_back(std::string(file.name) + (file.is_dir ? "/" : ""));
			}

			tinydir_next(&dir);
		}

		return res;
	}
}
