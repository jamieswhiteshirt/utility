#include "sampler.h"
#include <math.h>

namespace utility
{
	namespace
	{
		int mod(int a, int b)
		{
			int ret = a % b;
			if(ret < 0) ret += b;
			return ret;
		}
		int clamp(int i, int min, int max)
		{
			return i < max ? (i < min ? min : i) : (max - 1);
		}
	}

	namespace curve
	{
		float first(float f)
		{
			return 0.0f;
		}
		float last(float f)
		{
			return 1.0f;
		}
		float nearest(float f)
		{
			return f < 0.5f ? 0.0f : 1.0f;
		}
		float linear(float f)
		{
			return f;
		}
		float cubic(float f)
		{
			return f * f * (3 - f * 2);
		}
	}

	template<typename datatype>
	SamplerBase<datatype>::SamplerBase(datatype* data, bool repeat, curve_func curveFunc)
		: _data(data), _repeat(repeat), _curveFunc(curveFunc)
	{}
	template<typename datatype>
	SamplerBase<datatype>::~SamplerBase()
	{}
	template class SamplerBase<char>;
	template class SamplerBase<unsigned char>;
	template class SamplerBase<short>;
	template class SamplerBase<unsigned short>;
	template class SamplerBase<int>;
	template class SamplerBase<unsigned int>;
	template class SamplerBase<float>;

	template<typename datatype>
	datatype& Sampler1D<datatype>::_valueAt(int x) const
	{
		if(this->_repeat)
		{
			x = mod(x, _xw);
		}
		else
		{
			x = clamp(x, 0, _xw);
		}
		return this->_data[x];
	}
	template<typename datatype>
	Sampler1D<datatype>::Sampler1D(datatype* data, bool repeat, curve_func curveFunc, int xw)
		: SamplerBase<datatype>(data, repeat, curveFunc), _xw(xw)
	{}
	template<typename datatype>
	float Sampler1D<datatype>::operator()(float x) const
	{
		int xi0 = (int)floorf(x);
		int xi1 = xi0 + 1;

		float xm1 = this->_curveFunc(x - xi0);
		float xm0 = 1.0f - xm1;

		return _valueAt(xi0) * xm0 + _valueAt(xi1) * xm1;
	}
	template<typename datatype>
	datatype Sampler1D<datatype>::operator()(int x) const
	{
		return _valueAt(x);
	}
	template class Sampler1D<char>;
	template class Sampler1D<unsigned char>;
	template class Sampler1D<short>;
	template class Sampler1D<unsigned short>;
	template class Sampler1D<int>;
	template class Sampler1D<unsigned int>;
	template class Sampler1D<float>;

	template<typename datatype>
	datatype& Sampler2D<datatype>::_valueAt(int x, int y) const
	{
		if(this->_repeat)
		{
			x = mod(x, _xw);
			y = mod(y, _yw);
		}
		else
		{
			x = clamp(x, 0, _xw);
			y = clamp(y, 0, _yw);
		}
		return this->_data[x + y * _xw];
	}
	template<typename datatype>
	Sampler2D<datatype>::Sampler2D(datatype* data, bool repeat, curve_func curveFunc, int xw, int yw)
		: SamplerBase<datatype>(data, repeat, curveFunc), _xw(xw), _yw(yw)
	{}
	template<typename datatype>
	float Sampler2D<datatype>::operator()(float x, float y) const
	{
		int xi0 = (int)floorf(x);
		int xi1 = xi0 + 1;
		int yi0 = (int)floorf(y);
		int yi1 = yi0 + 1;

		float xm1 = this->_curveFunc(x - xi0);
		float xm0 = 1.0f - xm1;
		float ym1 = this->_curveFunc(y - yi0);
		float ym0 = 1.0f - ym1;

		return (_valueAt(xi0, yi0) * xm0 + _valueAt(xi1, yi0) * xm1) * ym0 +
			(_valueAt(xi0, yi1) * xm0 + _valueAt(xi1, yi1) * xm1) * ym1;
	}
	template<typename datatype>
	datatype Sampler2D<datatype>::operator()(int x, int y) const
	{
		return _valueAt(x, y);
	}
	template class Sampler2D<char>;
	template class Sampler2D<unsigned char>;
	template class Sampler2D<short>;
	template class Sampler2D<unsigned short>;
	template class Sampler2D<int>;
	template class Sampler2D<unsigned int>;
	template class Sampler2D<float>;

	template<typename datatype>
	datatype& Sampler3D<datatype>::_valueAt(int x, int y, int z) const
	{
		if(this->_repeat)
		{
			x = mod(x, _xw);
			y = mod(y, _yw);
			z = mod(z, _zw);
		}
		else
		{
			x = clamp(x, 0, _xw);
			y = clamp(y, 0, _yw);
			z = clamp(z, 0, _zw);
		}
		return this->_data[x + (y + z * _yw) * _xw];
	}
	template<typename datatype>
	Sampler3D<datatype>::Sampler3D(datatype* data, bool repeat, curve_func curveFunc, int xw, int yw, int zw)
		: SamplerBase<datatype>(data, repeat, curveFunc), _xw(xw), _yw(yw), _zw(zw)
	{}
	template<typename datatype>
	float Sampler3D<datatype>::operator()(float x, float y, float z) const
	{
		int xi0 = (int)floorf(x);
		int xi1 = xi0 + 1;
		int yi0 = (int)floorf(y);
		int yi1 = yi0 + 1;
		int zi0 = (int)floorf(z);
		int zi1 = zi0 + 1;

		float xm1 = this->_curveFunc(x - xi0);
		float xm0 = 1.0f - xm1;
		float ym1 = this->_curveFunc(y - yi0);
		float ym0 = 1.0f - ym1;
		float zm1 = this->_curveFunc(z - zi0);
		float zm0 = 1.0f - zm1;

		return ((_valueAt(xi0, yi0, zi0) * xm0 + _valueAt(xi1, yi0, zi0) * xm1) * ym0 +
			(_valueAt(xi0, yi1, zi0) * xm0 + _valueAt(xi1, yi1, zi0) * xm1) * ym1) * zm0 +
			((_valueAt(xi0, yi0, zi1) * xm0 + _valueAt(xi1, yi0, zi1) * xm1) * ym0 +
			(_valueAt(xi0, yi1, zi1) * xm0 + _valueAt(xi1, yi1, zi1) * xm1) * ym1) * zm1;
	}
	template<typename datatype>
	datatype Sampler3D<datatype>::operator()(int x, int y, int z) const
	{
		return _valueAt(x, y, z);
	}
	template class Sampler3D<char>;
	template class Sampler3D<unsigned char>;
	template class Sampler3D<short>;
	template class Sampler3D<unsigned short>;
	template class Sampler3D<int>;
	template class Sampler3D<unsigned int>;
	template class Sampler3D<float>;
}
