#include "debug.h"
#ifdef _WIN32

#include <Windows.h>
#endif

void Print(std::string str)
{
#ifdef _DEBUG
#ifdef _WIN32
	OutputDebugStringA((str + "\n").c_str());
#elif __linux__
	printf("%s\n",str.c_str());
#endif
#endif
}
