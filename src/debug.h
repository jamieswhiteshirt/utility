#ifndef DEBUG_H
#define DEBUG_H
#include <string>

#define STR(S) std::string(S)
#define TOSTR(S) std::to_string(S)

void Print(std::string);

#endif //DEBUG_H
