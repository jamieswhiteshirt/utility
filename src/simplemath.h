#ifndef SIMPLEMATH_H
#define SIMPLEMATH_H

#include <math.h>

template<typename datatype>
inline datatype lesser(datatype a, datatype b)
{
	return a < b ? a : b;
}
template<typename datatype>
inline datatype greater(datatype a, datatype b)
{
	return a > b ? a : b;
}

template<typename datatype>
inline datatype clamp(datatype n, datatype min, datatype max)
{
	return greater(min, lesser(n, max));
}

inline float roundf(float f)
{
	return f >= 0.0f ? floorf(f + 0.5f) : ceilf(f - 0.5f);
}

#endif
