#ifndef UTIL_GAMEEXCEPTION_H
#define UTIL_GAMEEXCEPTION_H

#include <string>
#include <string.h>
#include <stdexcept>
#include <vector>

//This type of exception will not stop the game and does not require the program to stop
#define EXCEPTION_LEVEL_INSIGNIFICANT 0
//This type of exception will stop the game and does not require the program to stop
#define EXCEPTION_LEVEL_ERROR 1
//This type of exception will stop the game and will require the program to stop
#define EXCEPTION_LEVEL_FATAL_ERROR 1

namespace utility
{
	class MetaException
	{
	private:
		const unsigned int _level;
		std::vector<std::pair<std::string, std::string>> _meta;

	public:
		MetaException(std::string what, unsigned int level = EXCEPTION_LEVEL_ERROR);
		MetaException(const MetaException& e);
		MetaException(const std::exception& e, unsigned int level = EXCEPTION_LEVEL_ERROR);
		MetaException(const std::runtime_error& e, unsigned int level = EXCEPTION_LEVEL_ERROR);

		std::string what() const;

		void addMeta(std::string key, std::string value);
		unsigned int getLevel() const;

		bool isInsignificantOrWorse() const;
		bool isErrorOrWorse() const;
		bool isFatalErrorOrWorse() const;
	};
}

#endif
