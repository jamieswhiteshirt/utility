#ifndef FILESYSTEM_H
#define FILESYSTEM_H

#include <string>
#include <string.h>
#include <vector>

namespace utility
{
	class FileSystem
	{
	private:
		static std::string _executableDirectory;
	public:
		static std::string getExecutableDirectory();

		static bool directoryExists(const char* directory);
		static bool makeDirectory(const char* directory);
		static std::vector<std::string> getFilesInDirectory(const char* directory, bool includeDirs=false);
	};
}

#endif
