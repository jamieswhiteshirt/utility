#ifndef SAMPLER_H
#define SAMPLER_H

namespace utility
{
	typedef float (*curve_func)(float f);

	namespace curve
	{
		float first(float f);
		float last(float f);
		float nearest(float f);
		float linear(float f);
		float cubic(float f);
	}
	
	template<typename datatype>
	class SamplerBase
	{
	protected:
		datatype* _data;
		const bool _repeat;
		const curve_func _curveFunc;

		SamplerBase(datatype* data, bool repeat, curve_func curveFunc);
		~SamplerBase();
	};

	template<typename datatype>
	class Sampler1D : public SamplerBase<datatype>
	{
	private:
		int _xw;

		datatype& _valueAt(int x) const;
	public:
		Sampler1D(datatype* data, bool repeat, curve_func interpolationFunc, int xw);

		float operator()(float x) const;
		datatype operator()(int x) const;
	};

	template<typename datatype>
	class Sampler2D : public SamplerBase<datatype>
	{
	private:
		int _xw;
		int _yw;

		datatype& _valueAt(int x, int y) const;
	public:
		Sampler2D(datatype* data, bool repeat, curve_func interpolationFunc, int xw, int yw);

		float operator()(float x, float y) const;
		datatype operator()(int x, int y) const;
	};

	template<typename datatype>
	class Sampler3D : public SamplerBase<datatype>
	{
	private:
		int _xw;
		int _yw;
		int _zw;

		datatype& _valueAt(int x, int y, int z) const;
	public:
		Sampler3D(datatype* data, bool repeat, curve_func interpolationFunc, int xw, int yw, int zw);

		float operator()(float x, float y, float z) const;
		datatype operator()(int x, int y, int z) const;
	};
}

#endif