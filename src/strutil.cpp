#include "strutil.h"
#include <algorithm>
#include <functional>
#include <cctype>
#include <locale>
#include <sstream>
#include "utf8.h"

namespace utility
{
	std::string ltrim(std::string s)
	{
		s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
		return s;
	}

	std::string rtrim(std::string s)
	{
		s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
		return s;
	}

	std::string trim(std::string s)
	{
		return ltrim(rtrim(s));
	}

	std::string replaceAll(std::string haystack, const std::string& needle, const std::string& straw)
	{
		std::string::size_type pos = 0;
		while((pos = haystack.find(needle, pos)) != std::string::npos)
		{
			haystack.replace(pos, needle.length(), straw);
			pos += straw.length();
		}
		return haystack;
	}

	std::string intersperse(std::string s, unsigned int c)
	{
		std::string res = "";
		std::string::iterator it = s.begin();
		while (it != s.end())
		{
			if (it != s.begin())
				utf8::append(c, back_inserter(res));
			unsigned cc = utf8::next(it, s.end());
			utf8::append(cc, back_inserter(res));
		}
		return res;
	}

	std::vector<std::string> split(std::string s, unsigned int c)
	{
		std::vector<std::string> res;
		std::string buffer;
		std::string::iterator it = s.begin();
		while (it != s.end())
		{
			unsigned int cc = utf8::next(it, s.end());
			if (cc == c)
			{
				res.push_back(buffer);
				buffer.clear();
			}
			else
				utf8::append(cc, back_inserter(buffer));
		}
		return res;
	}
}
