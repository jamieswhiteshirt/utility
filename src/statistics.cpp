#include "statistics.h"
#include <ctime>
#include <ratio>

using namespace std::chrono;

namespace utility
{
	namespace statistics
	{
		Module::Module(std::string name)
			: name(name), _min(0), _max(0), _minMaxIsSet(false)
		{}

		Module::~Module()
		{}

		void Module::begin()
		{
			_isRunning = true;
			_begin = std::chrono::steady_clock::now();
			_interval = duration_cast<nanoseconds>(_begin - _end);
			_sumInterval += _interval;
			_minInterval = _interval < _minInterval || !_minMaxIsSet ? _interval : _minInterval;
			_maxInterval = _interval > _maxInterval || !_minMaxIsSet ? _interval : _maxInterval;
		}

		void Module::done()
		{
			_end = std::chrono::steady_clock::now();
			_isRunning = false;
			_time = duration_cast<nanoseconds>(_end - _begin);
			_sum += _time;
			_min = _time < _min || !_minMaxIsSet ? _time : _min;
			_max = _time > _max || !_minMaxIsSet ? _time : _max;
			_minMaxIsSet = true;
			_ticks++;
		}

		double Module::interval()
		{
			return duration_cast<duration<double>>(_interval).count();
		}

		double Module::time()
		{
			return duration_cast<duration<double>>(_time).count();
		}

		double Module::min()
		{
			return duration_cast<duration<double>>(_min).count();
		}

		double Module::max()
		{
			return duration_cast<duration<double>>(_max).count();
		}

		double Module::avg()
		{
			return duration_cast<duration<double>>(_sum).count() / _ticks;
		}

		double Module::minInterval()
		{
			return duration_cast<duration<double>>(_minInterval).count();
		}

		double Module::maxInterval()
		{
			return duration_cast<duration<double>>(_maxInterval).count();
		}

		double Module::avgInterval()
		{
			return duration_cast<duration<double>>(_sumInterval).count() / _ticks;
		}
	}
}
