#ifndef UTIL_STATISTICS_H
#define UTIL_STATISTICS_H

#include <string>
#include <chrono>
#include <map>

using namespace std::chrono;

namespace utility
{
	namespace statistics
	{
		class Module
		{
		public:
			Module(std::string name);
			~Module();
			const std::string name;

			void begin();
			void done();

			double interval();
			double time();
			double min();
			double max();
			double avg();
			double minInterval();
			double maxInterval();
			double avgInterval();
		private:
			bool _isRunning;
			steady_clock::time_point _begin;
			steady_clock::time_point _end;

			nanoseconds _time;
			nanoseconds _interval;
			nanoseconds _min;
			nanoseconds _max;
			nanoseconds _sum;
			nanoseconds _minInterval;
			nanoseconds _maxInterval;
			nanoseconds _sumInterval;
			size_t _ticks;

			bool _minMaxIsSet;
		};
	}
}

#endif
