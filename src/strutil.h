#ifndef TRIM_H
#define TRIM_H

#include <string>
#include <string.h>
#include <vector>

namespace utility
{
	extern std::string ltrim(std::string s);
	extern std::string rtrim(std::string s);
	extern std::string trim(std::string s);

	extern std::string replaceAll(std::string haystack, const std::string& needle, const std::string& straw);
	extern std::string intersperse(std::string s, unsigned int c);
	extern std::vector<std::string> split(std::string s, unsigned int c);
};

#endif
