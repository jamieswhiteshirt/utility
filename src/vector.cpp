#define _USE_MATH_DEFINES

#include "vector.h"
#include <math.h>

namespace utility
{
	template<typename T>
	Vector3<T>::Vector3(T x, T y, T z)
		: _x(x), _y(y), _z(z)
	{}
	template<typename T>
	bool Vector3<T>::operator==(const Vector3<T>& v) const
	{
		return _x == v._x && _y == v._y && _z == v._z;
	}
	template<typename T>
	bool Vector3<T>::operator!=(const Vector3<T>& v) const
	{
		return !this->operator==(v);
	}
	template<typename T>
	Vector3<T> Vector3<T>::operator+(const Vector3<T> v) const
	{
		return Vector3<T>(_x + v._x, _y + v._y, _z + v._z);
	}
	template<typename T>
	Vector3<T>& Vector3<T>::operator+=(const Vector3<T> v)
	{
		_x += v._x;
		_y += v._y;
		_z += v._z;
		return *this;
	}
	template<typename T>
	Vector3<T> Vector3<T>::operator-(const Vector3<T> v) const
	{
		return Vector3<T>(_x - v._x, _y - v._y, _z - v._z);
	}
	template<typename T>
	Vector3<T>& Vector3<T>::operator-=(const Vector3<T> v)
	{
		_x -= v._x;
		_y -= v._y;
		_z -= v._z;
		return *this;
	}
	template<typename T>
	Vector3<T> Vector3<T>::operator*(const Vector3<T> v) const
	{
		return Vector3<T>(_x * v._x, _y * v._y, _z * v._z);
	}
	template<typename T>
	Vector3<T>& Vector3<T>::operator*=(const Vector3<T> v)
	{
		_x *= v._x;
		_y *= v._y;
		_z *= v._z;
		return *this;
	}
	template<typename T>
	Vector3<T> Vector3<T>::operator/(const Vector3<T> v) const
	{
		return Vector3<T>(_x / v._x, _y / v._y, _z / v._z);
	}
	template<typename T>
	Vector3<T>& Vector3<T>::operator/=(const Vector3<T> v)
	{
		_x /= v._x;
		_y /= v._y;
		_z /= v._z;
		return *this;
	}
	template<typename T>
	Vector3<T> Vector3<T>::operator-() const
	{
		return Vector3<T>(-_x, -_y, -_z);
	}
	template<typename T>
	Vector3<T> Vector3<T>::operator*(const T s) const
	{
		return Vector3<T>(_x * s, _y * s, _z * s);
	}
	template<typename T>
	Vector3<T>& Vector3<T>::operator*=(const T s)
	{
		_x *= s;
		_y *= s;
		_z *= s;
		return *this;
	}
	template<typename T>
	Vector3<T> Vector3<T>::operator/(const T s) const
	{
		return Vector3<T>(_x / s, _y / s, _z / s);
	}
	template<typename T>
	Vector3<T>& Vector3<T>::operator/=(const T s)
	{
		_x /= s;
		_y /= s;
		_z /= s;
		return *this;
	}
	template<typename T>
	float Vector3<T>::sqMagnitude() const
	{
		return _x * _x + _y * _y + _z * _z;
	}
	template<typename T>
	float Vector3<T>::magnitude() const
	{
		return sqrtf(sqMagnitude());
	}
	template<typename T>
	Vector3<T> Vector3<T>::normalized() const
	{
		return *this / magnitude();
	}
	template<typename T>
	T Vector3<T>::dot(const Vector3<T> u, const Vector3<T> v)
	{
		return u._x * v._x + u._y * v._y + u._z * v._z;
	}
	template<typename T>
	Vector3<T> Vector3<T>::cross(const Vector3<T> u, const Vector3<T> v)
	{
		return Vector3<T>(u._y * v._z - u._z * v._y, u._z * v._x - u._x * v._z, u._x * v._y - u._y * v._x);
	}

	template<typename T>
	Vector4<T>::Vector4(T x, T y, T z, T w)
		: _x(x), _y(y), _z(z), _w(w)
	{}
	template<typename T>
	Vector4<T> Vector4<T>::operator*(const T s) const
	{
		return Vector4<T>(_x * s, _y * s, _z * s, _w * s);
	}
	template<typename T>
	Vector4<T> Vector4<T>::operator*=(const T s)
	{
		_x *= s;
		_y *= s;
		_z *= s;
		_w *= s;
		return *this;
	}
	template<typename T>
	Vector4<T> Vector4<T>::operator/(const T s) const
	{
		return Vector4<T>(_x / s, _y / s, _z / s, _w / s);
	}
	template<typename T>
	Vector4<T> Vector4<T>::operator/=(const T s)
	{
		_x /= s;
		_y /= s;
		_z /= s;
		_w /= s;
		return *this;
	}

	Normal::Normal(float x, float y, float z)
		: Vector3f(x, y, z)
	{}

	Quaternion::Quaternion(float x, float y, float z, float w)
		: _x(x), _y(y), _z(z), _w(w)
	{}
	Quaternion Quaternion::operator*(const Quaternion& q) const
	{
		return Quaternion(this->_w * q._x + this->_x * q._w + this->_y * q._z - this->_z * q._y,
						  this->_w * q._y - this->_x * q._z + this->_y * q._w + this->_z * q._x,
						  this->_w * q._z + this->_x * q._y - this->_y * q._x + this->_z * q._w,
						  this->_w * q._w - this->_x * q._x - this->_y * q._y - this->_z * q._z);
	}
	Quaternion& Quaternion::operator*=(const Quaternion& q)
	{
		Quaternion result = *this * q;
		this->_x = result._x;
		this->_y = result._y;
		this->_z = result._z;
		this->_w = result._w;
		return *this;
	}
	Quaternion Quaternion::operator/(const Quaternion& q) const
	{
		return *this * q.conjugate();
	}
	Quaternion& Quaternion::operator/=(const Quaternion& q)
	{
		Quaternion result = *this / q;
		this->_x = result._x;
		this->_y = result._y;
		this->_z = result._z;
		this->_w = result._w;
		return *this;
	}
	Vector3f Quaternion::operator*(const Vector3f v) const
	{
		Vector3f u(_x, _y, _z);
		Vector3f t = Vector3f::cross(u, v) * 2.0f;
		return v + t * _w + Vector3f::cross(u, t);
	}
	Quaternion Quaternion::conjugate() const
	{
		return Quaternion(-_x, -_y, -_z, _w);
	}
	Quaternion Quaternion::slerp(Quaternion a, Quaternion b, float t)
	{
		float cosHalfTheta = a._x * b._x + a._y * b._y + a._z * b._z + a._w * b._w;

		if(fabsf(cosHalfTheta) >= 1.0f)
		{
			return a;
		}

		float halfTheta = acosf(cosHalfTheta);
		float sinHalfTheta = sqrtf(1.0f - cosHalfTheta * cosHalfTheta);

		float ra = sinf((1 - t) * halfTheta) / sinHalfTheta;
		float rb = sinf(t * halfTheta) / sinHalfTheta;

		return Quaternion(a._x * ra + b._x * rb, a._y * ra + b._y * rb, a._z * ra + b._z * rb, a._w * ra + b._w * rb);
	}
	Quaternion Quaternion::axisAngle(float x, float y, float z, float degrees)
	{
		float rad = degrees * (float)M_PI / 360.0f;
		float s = sinf(rad);
		return Quaternion(s * x, s * y, s * z, cosf(rad));
	}
	Quaternion Quaternion::axisAngle(Vector3f axis, float degrees)
	{
		return axisAngle(axis._x, axis._y, axis._z, degrees);
	}
	Quaternion Quaternion::fromEulerAngles(float x, float y, float z)
	{
		return axisAngle(1.0f, 0.0f, 0.0f, x) * axisAngle(0.0f, 1.0f, 0.0f, y) * axisAngle(0.0f, 0.0f, 1.0f, z);
	}
	Quaternion Quaternion::fromEulerAngles(Vector3f eulerAngles)
	{
		return fromEulerAngles(eulerAngles._x, eulerAngles._y, eulerAngles._z);
	}

	template struct Vector3<float>;
	template struct Vector3<int>;

	template struct Vector4<float>;
	template struct Vector4<int>;
}


