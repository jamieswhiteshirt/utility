#include "metaexception.h"
#include <sstream>
#include <ostream>

namespace utility
{
	MetaException::MetaException(std::string what, unsigned int level)
		:  _level(level)
	{
		addMeta("what", what);
	}
	MetaException::MetaException(const MetaException& e)
		: _meta(e._meta), _level(e._level)
	{}
	MetaException::MetaException(const std::exception& e, unsigned int level)
		: _level(level)
	{
		addMeta("what", e.what());
	}
	MetaException::MetaException(const std::runtime_error& e, unsigned int level)
		: _level(level)
	{
		addMeta("what", e.what());
	}

	std::string MetaException::what() const
	{
		std::ostringstream strOut;

		for(auto iter = _meta.begin(); iter != _meta.end(); ++iter)
		{
			strOut << ">> " << iter->first << ": " << iter->second << "\n";
		}

		return strOut.str();
	}

	void MetaException::addMeta(std::string key, std::string value)
	{
		_meta.push_back(std::pair<std::string, std::string>(key, value));
	}
	unsigned int MetaException::getLevel() const
	{
		return _level;
	}

	bool MetaException::isInsignificantOrWorse() const
	{
		return _level >= EXCEPTION_LEVEL_INSIGNIFICANT;
	}
	bool MetaException::isErrorOrWorse() const
	{
		return _level >= EXCEPTION_LEVEL_ERROR;
	}
	bool MetaException::isFatalErrorOrWorse() const
	{
		return _level >= EXCEPTION_LEVEL_FATAL_ERROR;
	}
}