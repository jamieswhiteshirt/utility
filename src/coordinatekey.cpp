#include "coordinatekey.h"

namespace utility
{
	CoordinateKey::CoordinateKey(int x, int y, int z)
		: x(x), y(y), z(z)
	{}
	bool CoordinateKey::operator==(const CoordinateKey& other) const
	{
		return x == other.x && y == other.y && z == other.z;
	}
	bool CoordinateKey::operator<(const CoordinateKey& other) const
	{
		if(x == other.x)
		{
			if(y == other.y)
			{
				return z < other.z;
			}
			else
			{
				return y < other.y;
			}
		}
		else
		{
			return x < other.x;
		}
	}

	CoordinateKey2D::CoordinateKey2D(int x, int y)
		: x(x), y(y)
	{}
	bool CoordinateKey2D::operator==(const CoordinateKey2D& other) const
	{
		return x == other.x && y == other.y;
	}
	bool CoordinateKey2D::operator<(const CoordinateKey2D& other) const
	{
		if(x == other.x)
		{
			return y < other.y;
		}
		else
		{
			return x < other.x;
		}
	}
}