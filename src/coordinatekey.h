#ifndef COORDINATEKEY_H
#define COORDINATEKEY_H

#include <unordered_map>

namespace utility
{
	struct CoordinateKey
	{
		int x, y, z;
		CoordinateKey(int x, int y, int z);
		bool operator==(const CoordinateKey& other) const;
		bool operator<(const CoordinateKey& other) const;
	};

	struct CoordinateKey2D
	{
		int x, y;
		CoordinateKey2D(int x, int y);
		bool operator==(const CoordinateKey2D& other) const;
		bool operator<(const CoordinateKey2D& other) const;
	};
}

namespace std
{
	using std::size_t;
	using std::hash;

	template<>
	struct hash<utility::CoordinateKey>
	{
		std::size_t operator()(const utility::CoordinateKey& key) const
		{
			return (hash<int>()(key.x) >> 8) ^ hash<int>()(key.y) ^ (hash<int>()(key.z) << 8);
		}
	};

	template<>
	struct hash<utility::CoordinateKey2D>
	{
		std::size_t operator()(const utility::CoordinateKey2D& key) const
		{
			return (hash<int>()(key.x) >> 8) ^ hash<int>()(key.y);
		}
	};
}

#endif