#ifndef VECTOR_H
#define VECTOR_H

namespace utility
{
	template<typename T=float>
	struct Vector4;

	template<typename T=float>
	struct Vector3
	{
		T _x, _y, _z;
		Vector3(T x = 0, T y = 0, T z = 0);
		template<typename U>
		Vector3(const Vector3<U>& t) : _x((T)t._x), _y((T)t._y), _z((T)t._z) {}
		template<typename U>
		Vector3(const Vector4<U>& t) : _x((T)t._x), _y((T)t._y), _z((T)t._z) {}
		

		bool operator==(const Vector3<T>& v) const;
		bool operator!=(const Vector3<T>& v) const;
		Vector3<T> operator+(const Vector3<T> v) const;
		Vector3<T>& operator+=(const Vector3<T> v);
		Vector3<T> operator-(const Vector3<T> v) const;
		Vector3<T>& operator-=(const Vector3<T> v);
		Vector3<T> operator*(const Vector3<T> s) const;
		Vector3<T>& operator*=(const Vector3<T> s);
		Vector3<T> operator/(const Vector3<T> s) const;
		Vector3<T>& operator/=(const Vector3<T> s);
		Vector3<T> operator-() const;
		Vector3<T> operator*(const T s) const;
		Vector3<T>& operator*=(const T s);
		Vector3<T> operator/(const T s) const;
		Vector3<T>& operator/=(const T s);

		float sqMagnitude() const;
		float magnitude() const;

		Vector3<T> normalized() const;

		static T dot(const Vector3<T> u, const Vector3<T> v);
		static Vector3<T> cross(const Vector3<T> u, const Vector3<T> v);
	};

	typedef Vector3<float> Vector3f;
	typedef Vector3<int> Vector3i;

	template<typename T>
	struct Vector4
	{
		T _x, _y, _z, _w;
		Vector4(T x = 0, T y = 0, T z = 0, T w = 1);
		template<typename U>
		Vector4(const Vector4<U>& t) : _x((T)t._x), _y((T)t._y), _z((T)t._z), _w((T)t._w) {}
		template<typename U>
		Vector4(const Vector3<U>& t, float w) : _x((T)t._x), _y((T)t._y), _z((T)t._z), _w(w) {}

		Vector4<T> operator*(const T s) const;
		Vector4<T> operator*=(const T s);
		Vector4<T> operator/(const T s) const;
		Vector4<T> operator/=(const T s);
	};

	typedef Vector4<float> Vector4f;
	typedef Vector4<int> Vector4i;

	class Normal : public Vector3f
	{
	public:
		Normal(float x = 0.0f, float y = 0.0f, float z = 0.0f);
	};

	class Quaternion
	{
	public:
		float _x, _y, _z, _w;

		Quaternion(float x = 0.0f, float y = 0.0f, float z = 0.0f, float w = 1.0f);
		Quaternion operator*(const Quaternion& q) const;
		Quaternion& operator*=(const Quaternion& q);
		Quaternion operator/(const Quaternion& q) const;
		Quaternion& operator/=(const Quaternion& q);
		Vector3f operator*(const Vector3f v) const;

		Quaternion conjugate() const;

		static Quaternion slerp(Quaternion a, Quaternion b, float t);
		static Quaternion axisAngle(float x, float y, float z, float degrees);
		static Quaternion axisAngle(Vector3f axis, float degrees);
		static Quaternion fromEulerAngles(float x, float y, float z);
		static Quaternion fromEulerAngles(Vector3f eulerAngles);
	};
}

#endif
